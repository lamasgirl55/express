import { Router } from "express";
import {
  createClassRoom,
  deleteClassRoomById,
  readAllClassRoom,
  readClassRoomById,
  updateClassRoomById,
} from "../controller/classRoomController.js";

const classRoomRoute = Router();

classRoomRoute.route("/").post(createClassRoom).get(readAllClassRoom);
classRoomRoute
  .route("/:ClassId")
  .get(readClassRoomById)
  .patch(updateClassRoomById)
  .delete(deleteClassRoomById);

export default classRoomRoute;
