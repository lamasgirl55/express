import { Router } from "express";
import {
  createCollege,
  deleteCollegeById,
  readAllCollege,
  readCollegeById,
  updateCollegeById,
} from "../controller/collegeController.js";

const collegeRoute = Router();

collegeRoute.route("/").post(createCollege).get(readAllCollege);
collegeRoute
  .route("/:CollegeId")
  .get(readCollegeById)
  .patch(updateCollegeById)
  .delete(deleteCollegeById);

export default collegeRoute;
