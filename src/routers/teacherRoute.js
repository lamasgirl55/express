import { Router } from "express";
import { createTeacher, deleteTeacherById, readAllTeachers, readTeacherById, updateTeacherById } from "../controller/teacherController.js";

const teacherRoute = Router();
//teacher.create()
//teacher.find({})
//teacher.findById(id)
//teacher.findByIdAndDelete(id)

teacherRoute
  .route("/")
  .post(createTeacher)
  .get(readAllTeachers);
teacherRoute
  .route("/:teacherId")
  .get(readTeacherById)
  .delete(deleteTeacherById)
  .patch(updateTeacherById);

export default teacherRoute;
