import { Router } from "express";
import {
  createShoes,
  deleteShoesById,
  readAllShoes,
  readShoesById,
  updateShoesById,
} from "../controller/shoesController.js";

const shoesRoute = Router();

shoesRoute.route("/")
.post(createShoes)
.get(readAllShoes);
shoesRoute
  .route("/:shoesId")
  .get(readShoesById)
  .patch(updateShoesById)
  .delete(deleteShoesById);

export default shoesRoute;
