import {Router} from "express"
export let foodRouter=Router()

foodRouter.route("/")
.post((req,res)=>{
      console.log(req.body)
      res.json({
            success:true,
            message:"food created successfully"})
})
//we can use multiple function[()=>{}] at once but to run all we have to call next to go down
//anything which has (req,res,next) is called middleware
//it is not necessary to call next()
//to hit another middleware we must call next function next()


.get((req,res,next)=>{
      console.log("i am fun1")
      next()
},(req,res,next)=>{
      console.log("i am fun2")
      next()
},(req,res,next)=>{
      console.log("i am fun3")
      res.json({
            success:true,
            message:"food read successfully"
      })
                      
})






// .get((req,res)=>{
//       res.json({
//             success:true,
//             message:"food read successfully"
//       })
// })
.patch((req,res)=>{
      res.json({
            success:true,
            message:"food updated successfully"
      })
})
.delete((req,res)=>{
      res.json({
            success:true,
            message:"food deleted successfully"
      })
})
//if id are same it gives first value from top
// it look from top to button and response which comes first
foodRouter.route("/:id") //localhost:8000/food/:id
.post((req,res)=>{
      console.log(req.params)
      console.log(req.params.id)
      res.json("i am food any")
})
foodRouter.route("/a")
.post((req,res)=>{
      res.json("hello")
})

//localhost:8000/food/any/ram/any
//post
//i am chhimi

foodRouter.route("/:id/ram/:id1")
.post((req,res)=>{
      console.log(req.params)
      console.log(req.query)
      res.json("i am chhimi")
})