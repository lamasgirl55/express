import { Router } from "express";
import { createRandom, deleteRandomById, readAllRandoms, readRandomById, updateRandomById } from "../controller/randomController.js";


const randomRoute = Router();
//random.create()
//random.find({})
//random.findById(id)
//random.findByIdAndDelete(id)

randomRoute
  .route("/")
  .post(createRandom)
  .get(readAllRandoms);
randomRoute
  .route("/:randomId")
  .get(readRandomById)
  .patch(updateRandomById)
  .delete(deleteRandomById)
  

export default randomRoute;
