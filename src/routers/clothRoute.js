import { Router } from "express";
import {
  createCloth,
  deleteClothById,
  readAllCloths,
  readClothById,
  updateClothById,
} from "../controller/clothController.js";
// import { createCloth, deleteClothById, readAllCloths, readClothById, updateClothById } from "../controller/createClothController.js";

const clothRoute = Router();
//cloth.create()
//cloth.find({})
//cloth.findById(id)
//cloth.findByIdAndDelete(id)

clothRoute.route("/").post(createCloth).get(readAllCloths);
clothRoute
  .route("/:clothId")
  .get(readClothById)
  .delete(deleteClothById)
  .patch(updateClothById);

export default clothRoute;
