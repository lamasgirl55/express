import { Router } from "express";
import {
  createDepartment,
  deleteDepartmentById,
  readAllDepartment,
  readDepartmentById,
  updateDepartmentById,
} from "../controller/departmentController.js";

const departmentRoute = Router();

departmentRoute.route("/").post(createDepartment).get(readAllDepartment);
departmentRoute
  .route("/:DepartmentId")
  .get(readDepartmentById)
  .patch(updateDepartmentById)
  .delete(deleteDepartmentById);

export default departmentRoute;
