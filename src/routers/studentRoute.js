import { Router } from "express";
import {
  createStudent,
  deleteStudentById,
  readAllStudent,
  readStudentByID,
  updateStudentById,
} from "../controller/studentController.js";

const studentRoute = Router();

studentRoute.route("/").post(createStudent).get(readAllStudent);
studentRoute
  .route("/:StudentId")
  .get(readStudentByID)
  .patch(updateStudentById)
  .delete(deleteStudentById);

export default studentRoute;
