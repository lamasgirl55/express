import { Router } from "express";
import {
  createBook,
  deleteBookById,
  readAllBook,
  readBookById,
  updateBookById,
} from "../controller/bookController.js";

const bookRoute = Router();

bookRoute.route("/").post(createBook).get(readAllBook);
bookRoute
  .route("/:BookId")
  .get(readBookById)
  .patch(updateBookById)
  .delete(deleteBookById);

export default bookRoute;
