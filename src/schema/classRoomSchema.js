//classRoom , name, number ofBench, hasTv
import { Schema } from "mongoose";

let classRoomSchema = Schema({
  name: {
    type: String,
    required: [true, "name field is required"],
  },

  numberOfBench: {
    type: Number,
    required: [true, "number of bench field is required"],
  },
  hasTv: {
    type: Boolean,
    required: [true, "hasTv field is required"],
  },
});

export default classRoomSchema;
