import { Schema } from "mongoose";
// define the content of object is called Schema
let teacherSchema = Schema({
  name: {
    type: String,
    required: [true, "name field is required"],
  },
  subject: {
    type: String,
    required: [true, "subject field is required"],
  },
  age: {
    type: Number,
    required: [true, "age field is required"],
  },
  isMarried: {
    type: Boolean,
    required: [true, "isMarried field is required"],
  },
});

export default teacherSchema;


//3)schema
//2)validation at schema
