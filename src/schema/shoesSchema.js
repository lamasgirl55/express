import { Schema } from "mongoose";

let shoesSchema = Schema({
  size: {
    type: Number,
    required: [true, "size field is required"],
  },
  price: {
    type: Number,
    required: [true, "price field is required"],
  },
  color: {
    type: String,
    required: [true, "color field is required"],
  },
  brand: {
    type: String,
    required: [true, "brand field is required"],
  },
  
});

export default shoesSchema;
