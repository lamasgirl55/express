//defining content of array is called model
//it is recommended
//to make singular and firstLetter capital of array name

import { model } from "mongoose";
import bookSchema from "./bookSchema.js";
import classRoomSchema from "./classRoomSchema.js";
import collegeSchema from "./collegeSchema.js";
import departmentSchema from "./departmentSchema.js";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import clothSchema from "./clothSchema.js";
import shoesSchema from "./shoesSchema.js";
import randomSchema from "./randomSchema.js";

//recommended to make same name of array and model
export let Teacher = model("Teacher", teacherSchema);
export let Book = model("Book", bookSchema);
export let Student = model("Student", studentSchema);
export let College = model("College", collegeSchema);
export let Class = model("Class", classRoomSchema);
export let Department = model("Department", departmentSchema);
export let Cloth = model("Cloth", clothSchema);
export let Shoes = model("Shoes", shoesSchema);
export let Random = model("Random", randomSchema);
