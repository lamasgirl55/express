//  {
//       "name": "c",
//       "age": 33,
//       "password":"Password@123",
//       "phoneNumber": 1111111111,
//       "roll": 2,
//       "isMarried": false,
//       "spouseName": "lkjl",
//       "email": "abc@gmai",
//       "gender": "male",
//       "dob": "2019-2-2",
//       "location": {
//           "country": "nepal",
//           "exactLocation": "gagalphedi"
//       },
// location: {
//       country: { type: String, required: [true, "country field is required"] },
//       exactLocation: {
//         type: String,
//         required: [true, "exactLocation field is required"],
//       },
//     },

//}
//       "favRandom": [
//           "a",
//           "b",
//           "c",
//           "nitan"
//       ],
// favTeacher: [
//       {
//         type: String,
//         required: [true, "favTeacher field is required"],
//       },
//     ],

//       "favSubject": [
//           {
//               "bookName": "javascript",
//               "bookAuthor": "nitan"
//           },
//           {
//               "bookName": "b",
//               "bookAuthor": "b"
//           }
//       ]
//   }
// favSubject: [
//       {
//         bookName: {
//           type: String,
//           required: [true, "bookName field is required"],
//         },
//         bookAuthor: {
//           type: String,
//           required: [true, "bookAuthor field is required"],
//         },
//       },
//     ],

import { Schema } from "mongoose";
// define the content of object is called Schema
let randomSchema = Schema({
  name: {
    type: String,

    //******manipulation of data*******
    //uppercase:true,
    //lowercase:true,
    //trim:true,
    //default:"maya"
    //*****validation of data*******
    //miniLength:[5,"name must be at least 5 character"],
    //maxLength:[10,"name must be at most 10 character"],
    required: [true, "name field is required"],
  },
  password: {
    type: String,
    required: [true, "password field is required"],
  },
  age: {
    type: Number,
    required: [true, "age field is required"],
    //min:[18,"age must be at least 18"]
    //max:[25,"age must be at most 25"]
  },
  isMarried: {
    type: Boolean,
    required: [true, "isMarried field is required"],
  },

  phoneNumber: {
    type: Number,
    required: [true, "phoneNumber field is required"],
  },

  roll: {
    type: Number,
    required: [true, "roll field is required"],
  },

  spouseName: {
    type: String,
    required: [true, "spouseName field is required"],
  },
  email: {
    type: String,
    required: [true, "email field is required"],
  },
  gender: {
    type: String,
    required: [true, "gender field is required"],
  },
  dob: {
    type: Date,
    required: [true, "dob field is required"],
  },
  location: {
    country: { type: String, required: [true, "country field is required"] },
    exactLocation: {
      type: String,
      required: [true, "exactLocation field is required"],
    },
  },

  favTeacher: [
    {
      type: String,
      required: [true, "favTeacher field is required"],
    },
  ],
  favSubject: [
    {
      bookName: {
        type: String,
        required: [true, "bookName field is required"],
      },
      bookAuthor: {
        type: String,
        required: [true, "bookAuthor field is required"],
      },
    },
  ],
});

export default randomSchema;
