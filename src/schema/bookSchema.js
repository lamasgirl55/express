import { Schema } from "mongoose";
//book name, author ,price, isAvailable

let bookSchema = Schema({
      name: {
        type: String,
        required: [true, "name field is required"],
      },
      price: {
        type: Number,
        required: [true, "price field is required"],
      },
      isAvailable: {
        type: Boolean,
        required: [true, "isAvailable field is required"],
      },
      author: {
        type: String,
        required: [false],
      },
    });
    
    export default bookSchema;


    //teacher with field name, age, isMarried, subject

//student name, class, faculty
//college  name, location, 
//classRoom , name, number ofBench, hasTv
//department name, hod, total member