import { Schema } from "mongoose";

const studentSchema = Schema({
      name: {
        type: String,
        required: [true, "name field is required"],
      },
      
      age: {
        type: Number,
        required: [true, "age field is required"],
      },
      class: {
            type: Number,
            required: [true, "class field is required"],
          },
      faculty: {
        type: String,
        required: [true, "faculty field is required"],
      },
    });
    
    export default studentSchema;