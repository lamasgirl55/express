import { Random } from "../schema/model.js";


export const createRandom=async (req, res) => {
      let data = req.body;
      try {
        let result = await Random.create(data);
        res.json({
          success: true,
          message: "Random created successfully",
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
    }

export const readAllRandoms=async (req, res) => {
      try {
        let results = await Random.find({});
        res.json({
          success: true,
          message: "random data read successfully",
          data: results,
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
      
    }    

export const readRandomById=async (req, res) => {
      let randomId = req.params.randomId;
      try {
        let result = await Random.findById(randomId);
        res.json({
          success: true,
          message: "random data read successfully",
          data: result,
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
    }    

export const updateRandomById=async (req, res) => {
      let randomId = req.params.randomId;
      let data=req.body
      try {
        let result = await Random.findByIdAndUpdate(randomId,data,{
          new:true
        });
        res.json({
          success: true,
          message: "random data update successfully",
          data: result,
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
    }    

export const deleteRandomById=async (req, res) => {
      let randomId = req.params.randomId;
      try {
        let result = await Random.findByIdAndDelete(randomId);
        res.json({
          success: true,
          message: "random data deleted successfully",
          data: result,
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
    }    