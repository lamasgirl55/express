// import { Cloth } from "../schema/model.js";

import { Cloth } from "../schema/model.js";

export const createCloth=async (req, res) => {
      let data = req.body;
      try {
        let result = await Cloth.create(data);
        res.json({
          success: true,
          message: "Cloth created successfully",
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
    }

export const readAllCloths=async (req, res) => {
      try {
        let results = await Cloth.find({});
        res.json({
          success: true,
          message: "cloth data read successfully",
          data: results,
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
      
    }    

export const readClothById=async (req, res) => {
      let clothId = req.params.clothId;
      try {
        let result = await Cloth.findById(clothId);
        res.json({
          success: true,
          message: "cloth data read successfully",
          data: result,
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
    }    

export const updateClothById=async (req, res) => {
      let clothId = req.params.clothId;
      let data=req.body
      try {
        let result = await Cloth.findByIdAndUpdate(clothId,data,{
          new:true
        });
        res.json({
          success: true,
          message: "cloth data update successfully",
          data: result,
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
    }    

export const deleteClothById=async (req, res) => {
      let clothId = req.params.clothId;
      try {
        let result = await Cloth.findByIdAndDelete(clothId);
        res.json({
          success: true,
          message: "cloth data deleted successfully",
          data: result,
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
    }    