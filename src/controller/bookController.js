import { Book } from "../schema/model.js";

export const createBook = async (req, res) => {
  let data = req.body;
  try {
    let result = await Book.create(data);
    res.json({
      success: true,
      message: "Book created successfully",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export const readAllBook = async (req, res) => {
  try {
    let results = await Book.find({});
    res.json({
      success: true,
      message: "Book data read successfully",
      data: results,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
  res.json("Book get");
};
export const readBookById = async (req, res) => {
  let BookId = req.params.BookId;
  try {
    let result = await Book.findById(BookId);
    res.json({
      success: true,
      message: "Book data read successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export const updateBookById = async (req, res) => {
  let BookId = req.params.BookId;
  let data = req.body;
  try {
    let result = await Book.findByIdAndUpdate(BookId, data, {
      new: true,
    });
    res.json({
      success: true,
      message: "Book data updated successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export const deleteBookById = async (req, res) => {
  let BookId = req.params.BookId;
  try {
    let result = await Book.findByIdAndDelete(BookId);
    res.json({
      success: true,
      message: "Book data deleted successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
