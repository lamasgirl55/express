import { Shoes } from "../schema/model.js";

export const createShoes = async (req, res) => {
  let data = req.body;
  try {
    let result = await Shoes.create(data);
    res.json({
      success: true,
      message: "Shoes created successfully",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const readAllShoes = async (req, res) => {
  try {
    let results = await Shoes.find({});
    res.json({
      success: true,
      message: "shoes data read successfully",
      data: results,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const readShoesById = async (req, res) => {
  let shoesId = req.params.shoesId;
  try {
    let result = await Shoes.findById(shoesId);
    res.json({
      success: true,
      message: "shoes data read successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const updateShoesById = async (req, res) => {
  let shoesId = req.params.shoesId;
  let data = req.body;
  try {
    let result = await Shoes.findByIdAndUpdate(shoesId, data, {
      new: true,
    });
    res.json({
      success: true,
      message: "shoes data update successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const deleteShoesById = async (req, res) => {
  let shoesId = req.params.shoesId;
  try {
    let result = await Shoes.findByIdAndDelete(shoesId);
    res.json({
      success: true,
      message: "shoes data deleted successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
