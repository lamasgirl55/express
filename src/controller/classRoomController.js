import { Class } from "../schema/model.js";

export const createClassRoom = async (req, res) => {
  let data = req.body;
  try {
    let result = await Class.create(data);
    res.json({
      success: true,
      message: "Class created successfully",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export const readAllClassRoom = async (req, res) => {
  try {
    let results = await Class.find({});
    res.json({
      success: true,
      message: "Class data read successfully",
      data: results,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
  res.json("Class get");
};
export const readClassRoomById = async (req, res) => {
  let ClassId = req.params.ClassId;
  try {
    let result = await Class.findById(ClassId);
    res.json({
      success: true,
      message: "Class data read successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export const updateClassRoomById = async (req, res) => {
  let ClassId = req.params.ClassId;
  let data = req.body;
  try {
    let result = await Class.findByIdAndUpdate(ClassId, data, {
      new: true,
    });
    res.json({
      success: true,
      message: "Class data updated successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export const deleteClassRoomById = async (req, res) => {
  let ClassId = req.params.ClassId;
  try {
    let result = await Class.findByIdAndDelete(ClassId);
    res.json({
      success: true,
      message: "Class data deleted successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
