import { Student } from "../schema/model.js";

export const createStudent=async (req, res) => {
      let data = req.body;
      try {
        let result = await Student.create(data);
        res.json({
          success: true,
          message: "Student created successfully",
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
    }

export const readAllStudent=    async (req, res) => {
      try {
        let results = await Student.find({});
        res.json({
          success: true,
          message: "Student data read successfully",
          data: results,
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
      res.json("Student get");
    }

export const readStudentByID=   async (req, res) => {
      let StudentId = req.params.StudentId;
      try {
        let result = await Student.findById(StudentId);
        res.json({
          success: true,
          message: "Student data read successfully",
          data: result,
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
    } 

export const updateStudentById=async (req, res) => {
      let StudentId = req.params.StudentId;
      let data = req.body;
      try {
        let result = await Student.findByIdAndUpdate(StudentId, data,{
          new:true
        });
        res.json({
          success: true,
          message: "Student data updated successfully",
          data: result,
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
    }

export const deleteStudentById=async (req, res) => {
      let StudentId = req.params.StudentId;
      try {
        let result = await Student.findByIdAndDelete(StudentId);
        res.json({
          success: true,
          message: "Student data deleted successfully",
          data: result,
        });
      } catch (error) {
        res.json({
          success: false,
          message: error.message,
        });
      }
    }  