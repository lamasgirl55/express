import { Department } from "../schema/model.js";

export const createDepartment = async (req, res) => {
  let data = req.body;
  try {
    let result = await Department.create(data);
    res.json({
      success: true,
      message: "Department created successfully",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export const readAllDepartment = async (req, res) => {
  try {
    let results = await Department.find({});
    res.json({
      success: true,
      message: "Department data read successfully",
      data: results,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
  res.json("Department get");
};
export const readDepartmentById = async (req, res) => {
  let DepartmentId = req.params.DepartmentId;
  try {
    let result = await Department.findById(DepartmentId);
    res.json({
      success: true,
      message: "Department data read successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export const updateDepartmentById = async (req, res) => {
  let DepartmentId = req.params.DepartmentId;
  let data = req.body;
  try {
    let result = await Department.findByIdAndUpdate(DepartmentId, data, {
      new: true,
    });
    res.json({
      success: true,
      message: "Department data updated successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export const deleteDepartmentById = async (req, res) => {
  let DepartmentId = req.params.DepartmentId;
  try {
    let result = await Department.findByIdAndDelete(DepartmentId);
    res.json({
      success: true,
      message: "Department data deleted successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
