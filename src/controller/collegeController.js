import { College } from "../schema/model.js";

export const createCollege = async (req, res) => {
  let data = req.body;
  try {
    let result = await College.create(data);
    res.json({
      success: true,
      message: "College created successfully",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const readAllCollege = async (req, res) => {
  try {
    let results = await College.find({});
    res.json({
      success: true,
      message: "College data read successfully",
      data: results,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
  res.json("College get");
};
export const readCollegeById = async (req, res) => {
  let CollegeId = req.params.CollegeId;
  try {
    let result = await College.findById(CollegeId);
    res.json({
      success: true,
      message: "College data read successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export const updateCollegeById = async (req, res) => {
  let CollegeId = req.params.CollegeId;
  let data = req.body;
  try {
    let result = await College.findByIdAndUpdate(CollegeId, data, {
      new: true,
    });
    res.json({
      success: true,
      message: "College data updated successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export const deleteCollegeById = async (req, res) => {
  let CollegeId = req.params.CollegeId;
  try {
    let result = await College.findByIdAndDelete(CollegeId);
    res.json({
      success: true,
      message: "College data deleted successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
