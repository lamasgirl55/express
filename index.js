import express, { json } from "express";
import { port } from "./src/constant.js";
import { firstRouter } from "./src/routers/firstRouter.js";
import { foodRouter } from "./src/routers/foodRouter.js";
import connectDb from "./src/connectdb/connectmongodb.js";
import teacherRoute from "./src/routers/teacherRoute.js";
import studentRoute from "./src/routers/studentRoute.js";
import bookRoute from "./src/routers/bookRoute.js";
import collegeRoute from "./src/routers/collegeRoute.js";
import departmentRoute from "./src/routers/departmentRoute.js";
import classRoomRoute from "./src/routers/classRoomRoute.js";
import clothRoute from "./src/routers/clothRoute.js";
import shoesRoute from "./src/routers/shoesRoute.js";
import randomRoute from "./src/routers/randomRoute.js";

//make expressApp
let expressApp = express();
expressApp.use(json());
connectDb();
// expressApp.use((req,res,next)=>{
//   console.log("**********")
//   next()
// })
//attached port to that expressApp
expressApp.listen(port, () => {
  console.log(`express app is listening at port port ${port}`);
});
expressApp.use("/", firstRouter); //localhost:8000
expressApp.use("/food", foodRouter); //localhost:8000/food
expressApp.use("/teachers", teacherRoute); //localhost:8000/teachers
expressApp.use("/students", studentRoute); //localhost:8000/students
expressApp.use("/books", bookRoute); //localhost:8000/books
expressApp.use("/college", collegeRoute); //localhost:8000/college
expressApp.use("/departments", departmentRoute); //localhost:8000/departments
expressApp.use("/classRooms", classRoomRoute); //localhost:8000/departments
expressApp.use("/cloths", clothRoute); //localhost:8000/departments
expressApp.use("/Shoes", shoesRoute); //localhost:8000/shoes
expressApp.use("/Random", randomRoute); //localhost:8000/shoes













































































//we have 2 middleware
//1)normal                2)error
//a)application
//b)route

//in normal middleware it has req,res,next
// to hit normal middleware we have to call next()
//in error middleware it has err,req,res,next
// to hit error middleware we have to call next(a)

//get params
//get query
//middle ware =>1)application=> it was use in index File,2)route=> it was use in route
//

//api(backend)=> defining task for each request
